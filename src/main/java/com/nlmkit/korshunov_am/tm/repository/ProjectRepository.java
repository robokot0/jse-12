package com.nlmkit.korshunov_am.tm.repository;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Репозитарий проектов
 */
public class ProjectRepository {
    /**
     * Список проектов
     */
    private List<Project> projects = new ArrayList<>();

    /**
     * Создать проект
     * @param name Имя проекта
     * @param userId ид пользователя
     * @return созданный проект
     */
    public Project create(final String name,final Long userId) {
        final Project project = new Project(name,userId);
        projects.add(project);
        return project;
    }

    /**
     * Создать проект
     * @param name Имя
     * @param description Описание
     * @return созданный проект
     */
    public Project create(final String name,final String description,final Long userId) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projects.add(project);
        return project;
    }

    /**
     * Изменить проект
     * @param id Идентификатор
     * @param name Имя
     * @param description Описание
     * @return проект
     */
    public Project update(final Long id,final String name,final String description,final Long userId) {
        final Project project = findById(id);
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return project;
    }

    /**
     * Очистить список проектов
     */
    public void clear() {
        projects.clear();
    }

    /**
     * Очистить список проектов пользователя
     */
    public void clear(final Long userId) {
        projects.removeIf(n->n.getUserId().equals(userId));
    }

    /**
     * Поиск проекта по индексу
     * @param index Индекс
     * @return проект если не найдено null
     */
    public Project findByIndex(final int index){
        return  projects.get(index);
    }

    /**
     * Поиск проекта по индексу и пользователю
     * @param index Индекс
     * @param userId ид пользователя
     * @return проект если не найдено null
     */
    public Project findByIndex(final int index,final Long userId){
        if (userId==null) return  null;
        Project project=projects.get(index);
        if (project==null) return  null;
        if (!userId.equals(project.getUserId())) return  null;
        return  project;
    }

    /**
     * Поиск проекта по имени
     * @param name имя
     * @return проект если не найдено null
     */
    public Project findByName(final String name){
        for (final Project project: projects) {
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    /**
     * Поиск проекта по имени и пользователю
     * @param name имя
     * @param userId ид пользователя
     * @return проект если не найдено null
     */
    public Project findByName(final String name,final Long userId){
        if (userId==null) return  null;
        for (final Project project: projects) {
            if (project.getName().equals(name) && project.getUserId().equals(userId)) return project;
        }
        return null;
    }

    /**
     * Поиск проекта по идентификатору
     * @param id идентификатор
     * @return проект если не найдено null
     */
    public Project findById(final Long id){
        for (final Project project: projects) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    /**
     * Поиск проекта по идентификатору и пользователю
     * @param id идентификатор
     * @param userId ид пользователя
     * @return проект если не найдено null
     */
    public Project findById(final Long id,final Long userId){
        if (userId==null) return  null;
        for (final Project project: projects) {
            if (project.getId().equals(id) && project.getUserId().equals(userId)) return project;
        }
        return null;
    }

    /**
     * Удалить проект по индексу
     * @param index индекс
     * @return удаленный проект если не найдено null
     */
    public Project removeByIndex(final int index){
        final Project project = findByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    /**
     * Удалить проект по индексу и пользователю
     * @param index индекс
     * @param userId ид пользователя
     * @return удаленный проект если не найдено null
     */
    public Project removeByIndex(final int index,final Long userId){
        if (userId==null) return  null;
        final Project project = findByIndex(index,userId);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    /**
     * Удалить проект по идентификатору
     * @param id  идентификатор
     * @return удаленный проект если не найдено null
     */
    public Project removeById(final Long id){
        final Project project = findById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    /**
     * Удалить проект по идентификатору и пользователю
     * @param id  идентификатор
     * @param userId ид пользователя
     * @return удаленный проект если не найдено null
     */
    public Project removeById(final Long id,final Long userId){
        if (userId==null) return  null;
        final Project project = findById(id,userId);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    /**
     * Удалить проект по имени
     * @param name имя
     * @return удаленный проект если не найдено null
     */
    public Project removeByName(final String name){
        final Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    /**
     * Удалить проект по имени  и пользователю
     * @param name имя
     * @param userId ид пользователя
     * @return удаленный проект если не найдено null
     */
    public Project removeByName(final String name,final Long userId){
        if (userId==null) return  null;
        final Project project = findByName(name,userId);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    /**
     * Получить список всех проектов
     * @return список проектов
     */
    public List<Project> findAll() {
        return projects;
    }

    /**
     * Получить список всех проектов пользователя
     * @param userId ид пользователя
     * @return список проектов
     */
    public List<Project> findAll(final Long userId) {
        return projects.stream().filter(p -> p.getUserId().equals(userId)).collect(Collectors.toList());
    }

    /**
     * Получить количество проектов в репозитарии
     * @return количество проектов
     */
    public int size() {
        return projects.size();
    }
    /**
     * Получить количество проектов  пользователя в репозитарии
     * @param userId ид пользователя
     * @return количество проектов
     */
    public int size(final Long userId) {
        return projects.stream().filter(p -> p.getUserId().equals(userId)).collect(Collectors.toList()).size();
    }
}
