package com.nlmkit.korshunov_am.tm.controller;

import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.service.ProjectTaskService;
import com.nlmkit.korshunov_am.tm.service.TaskService;

import java.util.List;

/**
 * Контроллер задач
 */
public class TaskController extends AbstractController {
    /**
     * Сервис задач
     */
    private final TaskService taskService;
    /**
     * Сервис задач в проекте
     */
    private final ProjectTaskService projectTaskService;
    /**
     * Конструктор
     * @param taskService Сервис задач
     * @param projectTaskService Сервис задач в проекте
     */
    public TaskController(TaskService taskService, ProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }
    /**
     * Изменить задачу
     * @param task задача
     * @return 0 выполнено
     */
    public int updateTask(final Task task){
        System.out.println("Please enter task name: ");
        final String name = scanner.nextLine();
        System.out.println("Please enter task description: ");
        final String description = scanner.nextLine();
        taskService.update(task.getId(),name,description,task.getUserId());
        System.out.println("[OK]");
        return 0;
    }
    /**
     * Изменить задачу по индексу
     * @return 0 выполнено
     */
    public int updateTaskByIndex(){
        System.out.println("[UPDATE TASK BY INDEX]");
        if (!this.testAuthUser())return 0;
        System.out.println("Please enter task index: ");
        final int index = Integer.parseInt(scanner.nextLine())-1;
        final Task task = this.getUser().getRole()== Role.ADMIN ?
                taskService.findByIndex(index):taskService.findByIndex(index,this.getUser().getId());
        if (task == null) System.out.println("[FAIL]");
        else updateTask(task);
        return 0;
    }

    /**
     * Удалить задачу по имени
     * @return 0 выполнено
     */
    public int removeTaskByName(){
        System.out.println("[REMOVE TASK BY NAME]");
        if (!this.testAuthUser())return 0;
        System.out.println("Please enter task name: ");
        final String name = scanner.nextLine();
        final Task task = this.getUser().getRole()==Role.ADMIN ?
                taskService.removeByName(name):taskService.removeByName(name,this.getUser().getId());
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Удалить задачу по ID
     * @return 0 выполнено
     */
    public int removeTaskByID(){
        System.out.println("[REMOVE TASK BY ID]");
        if (!this.testAuthUser())return 0;
        System.out.println("Please enter task ID: ");
        final long id = scanner.nextLong();
        final Task task = this.getUser().getRole()==Role.ADMIN ?
                taskService.removeById(id):taskService.removeById(id,this.getUser().getId());
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    /**
     * Удалить задачу по имени
     * @return 0 выполнено
     */
    public int removeTaskByIndex(){
        System.out.println("[REMOVE TASK BY INDEX]");
        if (!this.testAuthUser())return 0;
        System.out.println("Please enter task index: ");
        final int index = scanner.nextInt()-1;
        final Task task = this.getUser().getRole()==Role.ADMIN ?
                taskService.removeByIndex(index):taskService.removeByIndex(index,this.getUser().getId());
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }
    /**
     * Создать задачу
     * @return 0 выполнено
     */
    public int createTask(){
        System.out.println("[CREATE TASK]");
        if (!this.testAuthUser())return 0;
        System.out.println("Please enter task name: ");
        final String name = scanner.nextLine();
        System.out.println("Please enter task description: ");
        final String description = scanner.nextLine();
        taskService.create(name,description,this.getUser().getId());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удалить все задачи
     * @return 0 выполнено
     */
    public int clearTask(){
        System.out.println("[CLEAR TASK]");
        if (!this.testAuthUser())return 0;
        if(this.getUser().getRole()==Role.ADMIN) taskService.clear();
        else taskService.clear(this.getUser().getId());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Показать информацию по задаче
     * @param task задача
     */
    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    /**
     * Показать задачу по индексу
     * @return 0 выполнено
     */
    public int viewTaskByIndex() {
        if (!this.testAuthUser())return 0;
        System.out.println("Enter, task index:");
        final  int index = scanner.nextInt() - 1;
        final  Task task = this.getUser().getRole()==Role.ADMIN ?
                taskService.findByIndex(index):taskService.findByIndex(index,this.getUser().getId());
        viewTask(task);
        return 0;
    }

    /**
     * Показать список задач
     * @param tasks список
     */
    public void viewTasks(List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task.getId()+ ": " + task.getName());
            index ++;
        }
    }

    /**
     * Показать список задач
     * @return 0 выполнено
     */
    public int listTask(){
        System.out.println("[LIST TASK]");
        if (!this.testAuthUser())return 0;
        if(this.getUser().getRole()==Role.ADMIN) viewTasks(taskService.findAll());
        else viewTasks(taskService.findAll(this.getUser().getId()));
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Показать список задач по идентфиикатору проекта
     * @return 0 выполнено
     */
    public int listTaskByProjectId(){
        System.out.println("[LIST TASK BY PROJECT ID]");
        if (!this.testAuthUser())return 0;
        System.out.println("Please enter project ID: ");
        final long projectId = Long.parseLong(scanner.nextLine());
        if(this.getUser().getRole()==Role.ADMIN) viewTasks(taskService.findAllByProjectId(projectId));
        else viewTasks(taskService.findAllByProjectId(projectId,this.getUser().getId()));
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Добавить задачу в проект по идентфиикатору
     * @return 0 выполнео
     */
    public int addTaskToProjectByIds(){
        System.out.println("Please enter project ID: ");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("Please enter task ID: ");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.addTaskToProject(projectId,taskId);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удалить задачу из проекта по идентификатору
     * @return 0 выполнено
     */
    public int removeTaskFromProjectByIds(){
        System.out.println("[REMOVE TASK FROM PROJECT BY ID]");
        if (!this.testAuthUser())return 0;
        System.out.println("Please enter project ID: ");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("Please enter task ID: ");
        final long taskId = Long.parseLong(scanner.nextLine());
        if(this.getUser().getRole()==Role.ADMIN) projectTaskService.removeTaskFromProject(projectId,taskId);
        else projectTaskService.removeTaskFromProject(projectId,taskId,this.getUser().getId());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Поменять ИД пользователя в задаче задачу искать по ИД
     * @param user Пользователь на ид которого менять
     * @return 0 выполнено
     */
    public int setTaskUserById(final User user){
        if (!this.testAdminUser())return 0;
        System.out.println("Please enter task ID: ");
        final long taskId = Long.parseLong(scanner.nextLine());
        final Task task = taskService.findById(taskId);
        if(task==null)return 0;
        taskService.update(task.getId(),task.getName(),task.getDescription(),user.getId());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Поменять ИД пользователя в задаче задачу искать по индексу
     * @param user Пользователь на ид которого менять
     * @return 0 выполнено
     */
    public int setTaskUserByIndex(final User user){
        if (!this.testAdminUser())return 0;
        System.out.println("Please enter task index: ");
        final int index = Integer.parseInt(scanner.nextLine())-1;
        final Task task = taskService.findByIndex(index);
        if(task==null)return 0;
        taskService.update(task.getId(),task.getName(),task.getDescription(),user.getId());
        System.out.println("[OK]");
        return 0;
    }
}
