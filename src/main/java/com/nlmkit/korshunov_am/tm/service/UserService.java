package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.repository.UserRepository;

import java.util.List;

public class UserService {
    /**
     * Репозитарий пользователей
     */
    private final UserRepository userRepository;

    /**
     * Конструтор
     * @param userRepository Репозитарий польозвателей
     */
    public UserService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Создать пользователя
     * @param login имя
     * @return пользователь
     */
    public User create(String login) {
        if (login == null || login.isEmpty()) return null;
        if (userRepository.findByLogin(login)!=null) return null;
        return userRepository.create(login);
    }

    /**
     * Создать пользователя
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public User create(final String login, final Role role, final String firstName, final String secondName, final String middleName, final String passwordHash) {
        if (login == null || login.isEmpty()) return null;
        if (userRepository.findByLogin(login)!=null) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        return userRepository.create(login,role,firstName,secondName,middleName,passwordHash);
    }
    /**
     * Изменить пользователя
     * @param id идентификатор
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public User update(final Long id,final String login, final Role role, final String firstName, final String secondName, final String middleName, final String passwordHash) {
        if (id == null) return null;
        if (login == null || login.isEmpty()) return null;
        if (userRepository.findById(id)==null) return null;
        if (!userRepository.findByLogin(login).getId().equals(id)) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        return userRepository.update(id,login,role,firstName,secondName,middleName,passwordHash);
    }
    /**
     * Изменить данные пользователя
     * @param id идентификатор
     * @param login login
     * @param role роль
     * @param firstName Имя
     * @param secondName Фамилия
     * @param middleName Отчество
     * @return пользователь
     */
    public User updateData(final Long id,final String login, final Role role, final String firstName, final String secondName, final String middleName) {
        if (id == null) return null;
        if (login == null || login.isEmpty()) return null;
        if (userRepository.findById(id)==null) return null;
        if (!(userRepository.findByLogin(login)==null))
          if (!userRepository.findByLogin(login).getId().equals(id)) return null;
        return userRepository.updateData(id,login,role,firstName,secondName,middleName);
    }
    /**
     * Изменить пароль пользователя
     * @param id идентификатор
     * @param passwordHash Хэш пароля
     * @return пользователь
     */
    public User updatePassword(final Long id, String passwordHash) {
        if (id == null) return null;
        if (userRepository.findById(id)==null) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        return userRepository.updatePasswordHash(id, passwordHash);
    }

    /**
     * Удалить всех пользователей.
     */
    public void clear() {
        userRepository.clear();
    }
    /**
     * Найти пользователя по id.
     * @param id
     * @return пользователь
     */
    public User findById(final Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }
    /**
     * Найти пользователя по login.
     * @param login
     * @return пользователь
     */
    public User findByLogin(String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }
    /**
     * Найти пользователя по индексу.
     * @param index
     * @return пользователь
     */
    public User findByIndex(final int index) {
        return userRepository.findByIndex(index);
    }
    /**
     * Удалить пользователя по id
     * @param id
     * @return login
     */
    public User removeById(final Long id) {
        if (id == null) return null;
        if (userRepository.findById(id)==null) return null;
        return userRepository.removeById(id);
    }
    /**
     * Получить список всех пользователей
     * @return список пользователей
     */
    public List<User> findAll() {
        return userRepository.findAll();
    }
}
